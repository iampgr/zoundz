import Foundation
import UIKit


var APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
func mainStoryboard() -> UIStoryboard{
    return UIStoryboard(name: "Main", bundle: nil)
}

func SET_NAVIGATION_BAR(){
    let navigationBarAppearace = UINavigationBar.appearance()
    navigationBarAppearace.tintColor = #colorLiteral(red: 0, green: 0.1176470588, blue: 0.2509803922, alpha: 1)
    navigationBarAppearace.backgroundColor = .white
    navigationBarAppearace.shadowImage = UIImage()
    navigationBarAppearace.setBackgroundImage(imageFromColor(color: .white), for: .default)
    navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0, green: 0.1176470588, blue: 0.2509803922, alpha: 1) , NSAttributedString.Key.font: UIFont(name: "ProximaNova-Bold", size: 20)!]
    navigationBarAppearace.backIndicatorImage = #imageLiteral(resourceName: "Back Button")
    navigationBarAppearace.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "Back Button")

}

func navigationDecorationWithDarkColor(controller: UIViewController) {
    controller.navigationController?.navigationBar.setBackgroundImage(imageFromColor(color: #colorLiteral(red: 0, green: 0.1176470588, blue: 0.2509803922, alpha: 1)), for: .default)
    controller.navigationController?.navigationBar.shadowImage = UIImage()
    controller.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    controller.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),NSAttributedString.Key.font: UIFont(name: "ProximaNova-Bold", size: 20)!]
}

func navigationDecorationWithLightColor(controller: UIViewController) {
    controller.navigationController?.navigationBar.setBackgroundImage(imageFromColor(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)), for: .default)
    controller.navigationController?.navigationBar.shadowImage = UIImage()
    controller.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0, green: 0.1176470588, blue: 0.2509803922, alpha: 1)
    controller.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0, green: 0.1176470588, blue: 0.2509803922, alpha: 1),NSAttributedString.Key.font: UIFont(name: "ProximaNova-Bold", size: 20)!]
}


func imageFromColor(color: UIColor) -> UIImage? {
    let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()
    context?.setFillColor(color.cgColor)
    context?.fill(rect)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
}
