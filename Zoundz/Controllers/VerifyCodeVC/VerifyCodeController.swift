//
//  VerifyCodeController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 7/29/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class VerifyCodeController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func verifyCodeAction(_ sender: UIButton) {
        if let vc =  mainStoryboard().instantiateViewController(withIdentifier: "AlertController")as? AlertController{
            self.present(vc, animated: true, completion: nil)
        }
    }
}
