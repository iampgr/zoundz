//  HomeController.swift
//  Zoundz
//  Created by UNITED IT SERVICES on 7/30/20.
//  Copyright © 2020 MacsenIT. All rights reserved.

import UIKit

class HomeController: UIViewController {
    
    @IBOutlet weak var stationCollectionView: UICollectionView!
    var titleArray = ["DOG","CAT","BIRD","RABBIT"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stationCollectionView.register(HomeCollectionCell.nib, forCellWithReuseIdentifier: HomeCollectionCell.identifier)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "Show_Subscription"), object: nil, queue: OperationQueue.current) { (notification) in
            if let vc = mainStoryboard().instantiateViewController(withIdentifier: "SubscriptionController")as? SubscriptionController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationDecorationWithDarkColor(controller: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationDecorationWithLightColor(controller: self)
    }
    
    @IBAction func sideMenuAction(_ sender: UIBarButtonItem) {
        if let vc = mainStoryboard().instantiateViewController(withIdentifier: "SideMenuController")as? SideMenuController{
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func favoriteButtonAction(_ sender: UIBarButtonItem) {
        if let vc = mainStoryboard().instantiateViewController(withIdentifier: "NoInternetController")as? NoInternetController{
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}


extension HomeController : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width / 2) - 10
        return CGSize( width: width, height: 205.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionCell.identifier, for: indexPath)as! HomeCollectionCell
        cell.categoryImageView.image = UIImage(imageLiteralResourceName: "\(indexPath.item)")
        cell.categoryNameLabel.text = self.titleArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vc = mainStoryboard().instantiateViewController(withIdentifier: "TrackListController")as? TrackListController{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
