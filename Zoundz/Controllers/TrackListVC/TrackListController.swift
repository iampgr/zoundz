//
//  TrackListController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 8/2/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class TrackListController: UIViewController {
    
    @IBOutlet weak var trackTableView: UITableView!
    
    @IBOutlet weak var scrollContentHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.trackTableView?.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        self.trackTableView.register(TrackCell.nib, forCellReuseIdentifier: TrackCell.identifier)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let observedObject = object as? UITableView , observedObject == self.trackTableView{
            self.setHeightOfTableContentView()
        }
    }
}

extension TrackListController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TrackCell.identifier, for: indexPath)as! TrackCell
        return cell
    }
    
    func setHeightOfTableContentView(){
        let tableViewHeight = self.trackTableView.contentSize.height
        self.scrollContentHeight.constant = 300 + tableViewHeight
        self.view.layoutIfNeeded()
    }

}
