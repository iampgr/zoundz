//
//  NoInternetController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 8/2/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class NoInternetController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tryAgainAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
