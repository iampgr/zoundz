//
//  SideMenuController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 7/31/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class SideMenuController: UIViewController {
    
    @IBOutlet weak var menuTableView: UITableView!
    let titleArray = ["Home","Rate Us","Share","Privacy Policy","About","Log out","Subscribe"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.menuTableView.register(SideMenuCell.nib, forCellReuseIdentifier: SideMenuCell.identifier)
        self.menuTableView.register(SubscribeNowTableCell.nib, forCellReuseIdentifier: SubscribeNowTableCell.identifier)
        self.menuTableView.tableFooterView = UIView()
    }
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SideMenuController : UITableViewDelegate , UITableViewDataSource , SubscribeDelegate{
    
    func subscribeNow(cell: SubscribeNowTableCell) {
        
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Show_Subscription"), object: nil, userInfo: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: SubscribeNowTableCell.identifier, for: indexPath)as! SubscribeNowTableCell
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuCell.identifier , for: indexPath)as! SideMenuCell
            cell.cellTitleLabel.text = self.titleArray[indexPath.row]
            cell.cellImageView.image = UIImage(imageLiteralResourceName: self.titleArray[indexPath.row])
            return cell
        }
    }
    
    
    
}
