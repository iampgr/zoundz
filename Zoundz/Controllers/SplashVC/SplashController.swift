//  SplashController.swift
//  Zoundz
//  Created by UNITED IT SERVICES on 7/28/20.
//  Copyright ©2020 MacsenIT. All rights reserved.

import UIKit

class SplashController: UIViewController {

    @IBOutlet var logoView: UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.animate(withDuration: 1, animations: {
            self.logoView.alpha = 1
        }){(completion) in
            if let vc = mainStoryboard().instantiateViewController(withIdentifier: "welcomeNav")as? UINavigationController{
                APP_DELEGATE.window?.rootViewController = vc
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}
