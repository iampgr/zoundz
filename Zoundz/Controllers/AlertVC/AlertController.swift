//
//  AlertController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 7/30/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class AlertController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        if let vc = mainStoryboard().instantiateViewController(withIdentifier: "homeNav")as? UINavigationController{
            APP_DELEGATE.window?.rootViewController = vc
        }
    }
    
}
