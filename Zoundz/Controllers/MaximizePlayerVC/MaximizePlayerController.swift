//
//  MaximizePlayerController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 8/2/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class MaximizePlayerController: UIViewController {

    @IBOutlet weak var nextTrackTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextTrackTableView.register(TrackCell.nib, forCellReuseIdentifier: TrackCell.identifier)
    }
    
    @IBAction func minimizePlayerAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension MaximizePlayerController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TrackCell.identifier, for: indexPath)as! TrackCell
        return cell
    }
    
    
    
    
    
}
