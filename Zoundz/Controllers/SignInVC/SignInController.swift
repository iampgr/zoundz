//
//  SignInController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 7/29/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class SignInController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func requestCodeAction(_ sender: UIButton) {
        if let vc = mainStoryboard().instantiateViewController(withIdentifier: "VerifyCodeController")as? VerifyCodeController{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
