//
//  MinimizePlayerController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 8/2/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class MinimizePlayerController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func fullMusicPlayerButtonAction(_ sender: UIButton) {
        
        if let vc = mainStoryboard().instantiateViewController(withIdentifier: "MaximizePlayerController")as? MaximizePlayerController{
            self.present(vc, animated: true, completion: nil)
        }
    }
}
