//
//  SubscriptionController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 8/2/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class SubscriptionController: UIViewController {

    @IBOutlet weak var subscriptionTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.subscriptionTableView.register(SubscriptionCell.nib, forCellReuseIdentifier: SubscriptionCell.identifier)
    }
}


extension SubscriptionController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SubscriptionCell.identifier, for: indexPath)as! SubscriptionCell
        return cell
    }
    
    
    
    
    
}
