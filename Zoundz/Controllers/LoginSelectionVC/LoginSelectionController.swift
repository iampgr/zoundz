//
//  LoginSelectionController.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 7/28/20.
//  Copyright © 2020 MacsenIT. All rights reserved.

import UIKit

class LoginSelectionController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    @IBAction func signupButtonAction(_ sender: UIButton) {
        if let vc = mainStoryboard().instantiateViewController(withIdentifier: "SignUpController")as? SignUpController{
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    @IBAction func signinButtonAction(_ sender: UIButton) {
        if let vc = mainStoryboard().instantiateViewController(withIdentifier: "SignInController")as? SignInController{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
