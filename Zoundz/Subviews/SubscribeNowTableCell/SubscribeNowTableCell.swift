//
//  SubscribeNowTableCell.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 7/31/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit
protocol SubscribeDelegate{
    func subscribeNow(cell : SubscribeNowTableCell)
}



class SubscribeNowTableCell: UITableViewCell {

    var delegate : SubscribeDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }

    @IBAction func subscribeNowButtonAction(_ sender: UIButton) {
        self.delegate?.subscribeNow(cell: self)
    }
}
