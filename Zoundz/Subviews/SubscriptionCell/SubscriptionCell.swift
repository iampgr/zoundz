//
//  SubscriptionCell.swift
//  Zoundz
//
//  Created by UNITED IT SERVICES on 8/2/20.
//  Copyright © 2020 MacsenIT. All rights reserved.
//

import UIKit

class SubscriptionCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }

}
